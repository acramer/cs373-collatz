#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

# Imports for HackerRank
# from sys import stdin, stdout

# ------------
# collatz_read
# ------------

peaks = [
    1,
    2,
    3,
    6,
    7,
    9,
    18,
    25,
    27,
    54,
    73,
    97,
    129,
    171,
    231,
    313,
    327,
    649,
    703,
    871,
    1161,
    2223,
    2463,
    2919,
    3711,
    6171,
    10971,
    13255,
    17647,
    23529,
    26623,
    34239,
    35655,
    52527,
    77031,
    106239,
    142587,
    156159,
    216367,
    230631,
    410011,
    511935,
    626331,
    837799,
]
peak_values = [
    1,
    2,
    8,
    9,
    17,
    20,
    21,
    24,
    112,
    113,
    116,
    119,
    122,
    125,
    128,
    131,
    144,
    145,
    171,
    179,
    182,
    183,
    209,
    217,
    238,
    262,
    268,
    276,
    279,
    282,
    308,
    311,
    324,
    340,
    351,
    354,
    375,
    383,
    386,
    443,
    449,
    470,
    509,
    525,
]
# Cache only stores odd numbers, so indicies (i) correspond to the number 2i + 1
SIZEOFCACHE = 500000
cache = [-1] * SIZEOFCACHE
# Add peak values to the cache in case they are hit
for p in range(len(peaks) - 1, -1, -1):
    if peaks[p] % 2 != 0:
        cache[peaks[p] // 2] = peak_values[p]


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """

    # Ensure i is the smaller number and j is the larger number
    if i > j:
        i, j = j, i
    assert i <= j

    assert i > 0
    assert i < 1000000
    assert j > 0
    assert j < 1000000

    # Check to see if a peak is in a given range
    for k in range(len(peaks) - 1, -1, -1):
        if peaks[k] in range(i, j + 1):
            return peak_values[k]

    largestCycleLength = 0
    for e in range(i, j + 1):
        newE = collatz(e)
        if newE > largestCycleLength:
            largestCycleLength = newE

    assert largestCycleLength > 0

    return largestCycleLength


def collatz(e: int) -> int:
    update_list: List[int] = []
    while e // 2 >= SIZEOFCACHE or cache[e // 2] < 0 or e % 2 == 0:
        update_list.insert(0, e)
        if e % 2 == 0:
            e = e // 2
        else:
            # Since I calculate lenghts by iterating though all elements in the update list...
            # I can no longer skip evens after an odd number
            e = 3 * e + 1
    # Calculate and insert lengths into the cache
    clen = cache[e // 2]
    for f in update_list:
        clen += 1
        # Reminder: cache only stores the first 500000 odd numbers
        if f % 2 != 0 and f // 2 < SIZEOFCACHE:
            cache[f // 2] = clen

    # Ensuring that clen does not return a value associated with a empty cache
    assert clen > 0

    return clen


# Old code that didn't take advantage of the cache
# def collatz_basic(e: int) -> int :
#     clen = 1
#     while(e > 1):
#         if e%2 == 0:
#             e=e//2
#         else:
#             e+=(e>>1)+1
#             clen += 1
#         clen += 1
#     return clen

# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)


# I used this to create the Meta Cache
# def list_peaks(a):
#     peaks = [1]
#     lengths = [0]
#     for i in range(2,a+1):
#         if collatz(i) > lengths[-1]:
#             peaks.append(i)
#             lengths.append(collatz(i))
#
#     return lengths

# For HackerRank
# Taking this out to satisfy Coverage
# if __name__ == '__main__':
#     collatz_solve(stdin, stdout)
