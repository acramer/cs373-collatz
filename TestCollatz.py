#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # 10 Additional Unit Tests
    # 1. Should Increment to a number greater than 1,000,000
    def test_eval_5(self):
        v = collatz_eval(900000, 900001)
        self.assertEqual(v, 189)

    # 2. Should Trigger Meta Cache
    def test_eval_6(self):
        v = collatz_eval(800000, 900001)
        self.assertEqual(v, 525)

    # 3. Should Trigger Inverted Range
    def test_eval_7(self):
        v = collatz_eval(900001, 900000)
        self.assertEqual(v, 189)

    # 4. Range of len 0
    def test_eval_8(self):
        v = collatz_eval(900001, 900001)
        self.assertEqual(v, 189)

    # 5.
    def test_eval_9(self):
        v = collatz_eval(1, 8)
        self.assertEqual(v, 17)

    # 6.
    def test_eval_10(self):
        v = collatz_eval(1, 54)
        self.assertEqual(v, 113)

    # 7.
    def test_eval_11(self):
        v = collatz_eval(1, 871)
        self.assertEqual(v, 179)

    # 8.
    def test_eval_12(self):
        v = collatz_eval(1, 23529)
        self.assertEqual(v, 282)

    # 9.
    def test_eval_13(self):
        v = collatz_eval(1, 230631)
        self.assertEqual(v, 443)

    # 10.
    def test_eval_14(self):
        v = collatz_eval(230632, 410011)
        self.assertEqual(v, 449)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
