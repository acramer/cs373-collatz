# CS373: Software Engineering Collatz Repo

* Name: Andrew Cramer

* EID: ac49725

* GitLab ID: acramer

* HackerRank ID: acramer

* Git SHA: 4025a62635a47996d611a58dc9a48d6f285d83f3

* GitLab Pipelines: https://gitlab.com/acramer/cs373-collatz/pipelines

* Estimated completion time: 12.5

* Actual completion time: 14

* Comments: I took out code that allowed it to run on HackerRank in order to satisfy Coverage.  The code is commented out.
